# Overview of control, data, and management plane

## Things to do

- [ ] Make a slide going over why we will talk about this early on (To have a semi idea of what the planes mean and how they function in relation to a traditional network and the componenets)
- [ ] Make a slide going over how traditional networks each network components handled their own planes
- [ ] A slide on the control plane and its purpose
- [ ] A slide on the protocols controlled by the control plane (layer 2, layer 3)
- [ ] A slide on the data plane and its purpose
- [ ] A slide on how the data plane forwards at the bit level
- [ ] A slide on the management plane and its purpose
