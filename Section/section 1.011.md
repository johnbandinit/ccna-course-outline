# broadcast domain, collision domain, CSMA/CA, CSMA/CD

## Things to do

- [ ] Create slides going over the terms broadcast/collision domain
- [ ] Go over how a broadcast domain is a single "network" that contains multiple end devices that can all communicate within a layer 2 segment that is not discontinuous
- [ ] Create slide about how a collision domain is only 1-1 communication is a typically just the link between two devices
- [ ] Create a slide going over CSMA/CA and CSMA/CD