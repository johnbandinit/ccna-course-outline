# Build EVE-NG on Windows and Ubuntu

## Things to do

- [ ] Make a slide going over emulation software
- [ ] Make a slide going over GNS3 vs EVE-NG vs Packet Tracer
- [ ] Make a slide going over EVE-NG and how it uses Type 2 Hypervisor to host a website that allows you to use system resources to virtualize routers
- [ ] Go over EVE-NG website show students how to navigate the site and what to read for help
- [ ] Now make video installing EVE-NG on ubuntu VM
- [ ] Make video installing on windows machine and once complete go over some basic functions of EVE-NG
- [ ] Make video installing winSCP and get a VIRL and IOL image up and running in EVE-NG, then make a basic network
- [ ] Now make a slide going over different terminal emulator programs and what they're used for
- [ ] Make a video showing how in the real world a terminal emulator program is used. Use the console port, and SSH, and at the end say we emulate this in EVE-NG
- [ ] Make video going over which console you should use when logging into EVE-NG, install securecrt and show how to make securecrt default ssh/telnet application
- [ ] Now lets make a video going over how to make a basic network in EVE-NG

