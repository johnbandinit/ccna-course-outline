# Describe network components

## Things to do

- [ ] Slide going over how the "network" or "enterprise" is made up of key infrastructure components/hardware and how we must be aware of all the potential hardware that we may have to interact with as a network technician
- [ ] Slide dedicated to routers
- [ ] Slide dedicated to switches
- [ ] Slide dedicated to firewalls
- [ ] slide dedicated to Access points | WLC
-  [ ] Slide dedicated to Controllers based networking components
- [ ] Slide dedicated to endpoint devices (servers, printers, computers, phones)
