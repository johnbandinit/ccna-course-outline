# Describe the what and why of networking

## Things to do

- [ ] Make a slide going over the basis of the networking role. Maintaining infrastructure, troubleshooting outages, ensuring availability of resources via network reachability. Make a diagram of a typicall office and where the networking/network administrator come into play.
- [ ] Make a slide going over a little history of the internet and how networking became its defined role with hardware like routers, hubs, switches, firewalls.
- [ ] Make a slide saying how this role ensures the internet is alive and well and make the student feel special about their interest in networking.