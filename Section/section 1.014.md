# IPv6 and everything about IPv6 and how it differs 

## Things to do

- [ ] Learn IPv6
- [ ] Go over IPv6 rules and operations
- [ ] Go over all the different types of IPv6 addresses
- [ ] How to configure IPv6 on CLI
