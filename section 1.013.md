# Describe IPv4, Private/Public, APIPA, Loopback, history of IPv4 and classless/classful

# Things to do

- [ ] Create a slide going over what an IP address is and why we need it/ use it in communicating across the internet
- [ ] create a slide on how an IP address is represented to humans the structure 4 bytes, 8bit sections, dotted decimal format, the binary chart for IP addressing
- [ ] Create a slide going over how we put ip addresses into networks to define our ranges and our own internal communications. 
- [ ] create slide on history of internet and how ipv4 was used and why the 4.7 billion cannot be used now. In this history go over how companies wanted there own entworks and then we kept expanding to the point where we almost ran out so we had to categorizing ips into classes, and then the classes went away and then we created private IP addresses
- [ ] Create a slide going over RFC 1918 and why we need it
- [ ] Go over the specific IP addresses like APIPA's, Loopbacks
- [ ] Create a slide on how to configue and check for your IP address