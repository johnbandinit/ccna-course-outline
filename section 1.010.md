# Describe MAC addresses and layer 2 protocol

## Things to do

- [ ] Go over what a MAC address is and what it is used for (BIA, hexadecimal)
- [ ] Go over how MACs are burned into the NIC and are used to encapsulate a frame for forwarding decisions made by a layer device like a switch
- [ ] ensure you go over how Layer 2 uses MAC addresses to communication within a layer 2 ethernet topology (LAN)
