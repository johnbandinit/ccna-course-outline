# Go over wireless shit and very high level, channels, frequency, SSID, WAPs, WLCs, 802.11. 2.4/5Ghz

## Things to do

- [ ] Create slides going over what RF is and what Wi-Fi means
- [ ] Create slide going over basic signal theory and introduce words like modulation, frequency, spectrum, radio, AM/FM, 
- [ ] Create a slide going over channels in WiFi and how they can overlap within a Wi-Fi signal range 
- [ ] Create a slide going over common wifi specific terms (SSID, WAP, WLC, Heat Map, Encryption)
- [ ] Create a slide going over the history of Wi-Fi and the changing standards with throughput