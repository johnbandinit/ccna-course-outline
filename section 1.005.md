# describe the hardware in network components

## Things to do

- [ ] Describe what hardware makes up a router
- [ ] Describe what hardware makes up a switch
- [ ] Describe how the hardware makes forwarding decision and how it differs between router and switch (ASICs, CPU, Memory, Storage)