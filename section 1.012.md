# Go over bits and bytes how computers communicate

## Things to do

- [ ] Create a slide talking about how computers talk, how CPUs generate cycles and how bits are the machine language
- [ ] Go over 1's and 0's and what they mean to computers
- [ ] create a slide covering how many bits go into a byte and how data is always broken down on the wire back into bits
- [ ] Create a slide covering how MBps and Mbps differ and how speeds are determined by bits/second not bytes