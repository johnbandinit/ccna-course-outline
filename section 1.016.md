# Describe Layer 4, TCP/UDP and port

## Things to do

- [ ] Create slide on TCP 
- [ ] Create slide on UDP
- [ ] Create slide on TCP vs UDP
- [ ] Create slides going over different ports and how theyre designated (IANA registered, RFC ports, public ports)
- [ ] Create slide going over well know protocols (ICMP, RTP, ESP)
- [ ] Wireshark captures of both TCP and UDP